package com.example.springbootjsp.manager;

import com.example.springbootjsp.dao.MailDetailsRepo;
import com.example.springbootjsp.dao.entity.MailDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailDetailsManager {

    private MailDetailsRepo mailDetailsRepo;

    @Autowired
    public MailDetailsManager(MailDetailsRepo mailDetailsRepo) {
        this.mailDetailsRepo = mailDetailsRepo;
    }

    public MailDetails save(MailDetails mailDetails)
    {
        return mailDetailsRepo.save(mailDetails);
    }

}
