package com.example.springbootjsp.controllers;

import com.example.springbootjsp.email.EmailSender;
import com.example.springbootjsp.dao.entity.MailDetails;
import com.example.springbootjsp.manager.MailDetailsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MailController {

    private final EmailSender emailSender;
    private final MailDetailsManager mailDetailsManager;

    @Autowired
    public MailController(EmailSender emailSender, MailDetailsManager mailDetailsManager) {
        this.emailSender = emailSender;
        this.mailDetailsManager = mailDetailsManager;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView user() {

        return new ModelAndView("mail", "command", new MailDetails());
    }

    @RequestMapping(value = "/confirmedSend", method = RequestMethod.POST)

    public String addUser(@ModelAttribute("SpringWeb") MailDetails details,
                          ModelMap model) {
        emailSender.send(details.getRecipient(), details.getSubject(), details.getContent());
        mailDetailsManager.save(details);
        model.addAttribute("recipient", details.getRecipient());
        model.addAttribute("subject", details.getSubject());
        model.addAttribute("content", details.getContent());
        return "confirmed";
    }

   @ExceptionHandler(Exception.class)
    public String handleException()
   {
       return "handleException";
   }
}
