package com.example.springbootjsp.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class EmailSenderImpl implements EmailSender {

    private JavaMailSender emailSender;

    @Autowired
    public EmailSenderImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }


    @Override
    public void send(String recipient, String subject, String content) {
        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(recipient);
            helper.setSubject(subject);
            helper.setText(content, true);
            System.out.println("Done");
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);

    }
}
