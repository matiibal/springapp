package com.example.springbootjsp.email;


public interface EmailSender {
   void send(String recipient, String subject, String content);
}
