package com.example.springbootjsp.dao;

import com.example.springbootjsp.dao.entity.MailDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailDetailsRepo extends CrudRepository<MailDetails, Integer> {
}
