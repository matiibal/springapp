<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
    <title>Sending emails</title>
</head>
<body>

<h2>Send email</h2>
<form:form method = "POST" action = "/confirmedSend">
    <table>
        <tr>
            <td><form:label path = "recipient">To</form:label></td>
            <td><form:input path = "recipient" /></td>
        </tr>
        <tr>
            <td><form:label path = "subject">Subject</form:label></td>
            <td><form:input path = "subject" /></td>
        </tr>
        <tr>
            <td><form:label path = "content">Content</form:label></td>
            <td><form:textarea path = "content" rows = "5" cols = "30" /></td>
        </tr>
        <tr>
            <td colspan = "2">
                <input type = "submit" value = "Send"/>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
